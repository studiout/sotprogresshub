//
//  SOTProgressHUB.m
//  SOTProgressHUB
//
//  Created by Oni_01 on 14/09/13.
//  Copyright (c) 2013 Andrea Altea. All rights reserved.
//

#import "SOTProgressHUB.h"
#import "UIImage+ImageEffects.h"

@interface SOTProgressHUB ()

//Hud Image
@property (strong, nonatomic) UIView *hud;
@property (strong, nonatomic) UIWindow *keyWindow;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIActivityIndicatorView *activityindicator;
@property (strong, nonatomic) UILabel *textLabel;

@end

@implementation SOTProgressHUB

//Init Methods
-(instancetype)init{
    self = [super init];
    if(self){
        //Instanzio la vista principale
        self.view = [[UIView alloc] init];
        [self.view setBackgroundColor:[UIColor clearColor]];
        [self.view setUserInteractionEnabled:NO];
        //[self.view setAlpha:0];
        [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        //Instanzio l'hud
        self.hud = [[UIView alloc] init];
        [self.hud setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:0.9]];
        [self.hud.layer setCornerRadius:16.0f];
        [self.hud setAlpha:0];
        [self.hud setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        //Add Hud in View
        [self.view addSubview:self.hud];
        
        
        //Constraints
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.hud
                                                                      attribute:NSLayoutAttributeCenterX
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.view
                                                                      attribute:NSLayoutAttributeCenterX
                                                                     multiplier:1
                                                                       constant:0];
        [self.view addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.hud
                                                  attribute:NSLayoutAttributeCenterY
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeCenterY
                                                 multiplier:1
                                                   constant:0];
        [self.view addConstraint:constraint];
        
        //Default Motion Effects
        [self setMotionEffectsActive:YES];
    }
    return self;
}

-(instancetype)initWithImage:(UIImage *)image{
    self = [self init];
    if(self){
        if([image respondsToSelector:@selector(imageWithRenderingMode:)]){
            image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        }
        self.imageView = [[UIImageView alloc] initWithImage:image];
        if([self.imageView respondsToSelector:@selector(setTintColor:)]){
            [self.imageView setTintColor:[UIColor colorWithWhite:0.2f alpha:1.0f]];
        }
        [self.imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        
        //Add Image View in HUD
        [self.hud addSubview:self.imageView];
        
        //Constraints in HUD
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-20-[_imageView]-20-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(_imageView)];
        [self.hud addConstraints:constraints];
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[_imageView]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:NSDictionaryOfVariableBindings(_imageView)];
        [self.hud addConstraints:constraints];
        
        
    }
    return self;
}

-(instancetype)initWithActivityIndicator{
    self = [self init];
    if(self){
        self.activityindicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.activityindicator setColor:[UIColor colorWithWhite:0.2f alpha:1.0f]];
        [self.activityindicator setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        //Add Activity Indicator in HUD
        [self.hud addSubview:self.activityindicator];
        
        //Constraints in HUD
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-20-[_activityindicator]-20-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(_activityindicator)];
        [self.hud addConstraints:constraints];
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[_activityindicator]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:NSDictionaryOfVariableBindings(_activityindicator)];
        [self.hud addConstraints:constraints];
    }
    return self;
}

//Configure Methods
-(void)setImage:(UIImage *)image{
    if(self.imageView){
        [self.imageView setImage:image];
        [self.view setNeedsUpdateConstraints];
    }
}

-(void)setText:(NSString *)text{
    if(!self.textLabel){
        self.textLabel = [[UILabel alloc] init];
        [self.textLabel setNumberOfLines:2];
        [self.textLabel setTextAlignment:NSTextAlignmentCenter];
        [self.textLabel setBackgroundColor:[UIColor clearColor]];
        [self.textLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.textLabel setTextColor:[UIColor colorWithWhite:0.2f alpha:1.0f]];
        [self.hud addSubview:self.textLabel];
        
        [self.hud removeConstraints:self.hud.constraints];
        
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.textLabel
                                                                      attribute:NSLayoutAttributeCenterX
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.hud
                                                                      attribute:NSLayoutAttributeCenterX
                                                                     multiplier:1
                                                                       constant:0];
        [self.hud addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.hud
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.hud
                                                  attribute:NSLayoutAttributeHeight
                                                 multiplier:1
                                                   constant:0];
        [self.hud addConstraint:constraint];
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(>=4)-[_textLabel]-(>=4)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(_textLabel)];
        [self.hud addConstraints:constraints];
        
        
        //Vertical Constraints
        if(self.activityindicator){
            //Constraints in HUD
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(>=20)-[_activityindicator]-(>=20)-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings(_activityindicator)];
            [self.hud addConstraints:constraints];
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=20)-[_activityindicator]-[_textLabel]-8-|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:NSDictionaryOfVariableBindings(_activityindicator, _textLabel)];
            [self.hud addConstraints:constraints];
            constraint = [NSLayoutConstraint constraintWithItem:self.activityindicator
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.hud
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1
                                                       constant:0];
            [self.hud addConstraint:constraint];
            
            constraint = [NSLayoutConstraint constraintWithItem:self.activityindicator
                                                      attribute:NSLayoutAttributeCenterY
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.hud
                                                      attribute:NSLayoutAttributeCenterY
                                                     multiplier:1
                                                       constant:0];
            [self.hud addConstraint:constraint];
            
        }else if (self.imageView){
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(>=20)-[_imageView]-(>=20)-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:NSDictionaryOfVariableBindings(_imageView)];
            [self.hud addConstraints:constraints];
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=20)-[_imageView]-[_textLabel]-8-|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:NSDictionaryOfVariableBindings(_imageView, _textLabel)];
            [self.hud addConstraints:constraints];
            constraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                      attribute:NSLayoutAttributeCenterX
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.hud
                                                      attribute:NSLayoutAttributeCenterX
                                                     multiplier:1
                                                       constant:0];
            [self.hud addConstraint:constraint];
            
            constraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                      attribute:NSLayoutAttributeCenterY
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.hud
                                                      attribute:NSLayoutAttributeCenterY
                                                     multiplier:1
                                                       constant:0];
            [self.hud addConstraint:constraint];
            
        }
    }
    [self.textLabel setText:text];
    [self.view setNeedsUpdateConstraints];
}

//Show Methods
-(void)show{
    //Ottengo il RootViewController
    self.keyWindow = [[[UIApplication sharedApplication] delegate] window];
        [self.keyWindow addSubview:self.view];
        [self.keyWindow setUserInteractionEnabled:NO];
        
        //Constraints in View
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[_view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(_view)];
        [self.keyWindow addConstraints:constraints];
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_view]|"
                                                              options:0
                                                              metrics:nil
                                                                views:NSDictionaryOfVariableBindings(_view)];
        [self.keyWindow addConstraints:constraints];
        [self.view layoutSubviews];
        
    //IOS7 INTERFACE
    if ([self.hud respondsToSelector:@selector(addMotionEffect:)]) {
        //SnapShotting
        UIGraphicsBeginImageContextWithOptions(self.hud.frame.size, NO, self.keyWindow.screen.scale);
        CGPoint position = self.hud.frame.origin;
        CGSize size = self.keyWindow.frame.size;
        [self.keyWindow drawViewHierarchyInRect:CGRectMake(-position.x,
                                                                     -position.y,
                                                                     size.width,
                                                                     size.height)
                                       afterScreenUpdates:YES];
        UIImage *background = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        UIImage *blurredBackground = [background applyLightEffect];
        [self.hud setBackgroundColor:[UIColor colorWithPatternImage:blurredBackground]];
        
        //Motion Effects
        if(self.motionEffects){
            UIInterpolatingMotionEffect *motionY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y"
                                                                                                   type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis ];
            
            
            motionY.maximumRelativeValue = @(25);
            motionY.minimumRelativeValue = @(-25);
            
            UIInterpolatingMotionEffect *motionX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x"
                                                                                                   type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis ];
            
            
            motionX.maximumRelativeValue = @(25);
            motionX.minimumRelativeValue = @(-25);
            
            UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
            [group setMotionEffects:@[motionX, motionY]];
            
            [self.hud addMotionEffect:motionX];
            [self.hud addMotionEffect:motionY];
        }
    }
    
    //Show view
    [UIView animateWithDuration:0.1
                     animations:^{
                         [self.view setBackgroundColor:[UIColor colorWithWhite:0.3 alpha:0.4]];
                     }];
    //Show Hud
    [UIView animateWithDuration:0.3
                          delay:0.05
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.hud setAlpha:1];
                     }completion:^(BOOL finished) {
                         if(self.activityindicator){
                             [self.activityindicator startAnimating];
                         }
                     }];
    
}

-(void)showForTimeInterval:(NSTimeInterval)timeInterval{
    [self show];
    [self performSelector:@selector(remove) withObject:nil afterDelay:0.4+timeInterval];
}

//Remove Methods
-(void)remove{
    [UIView animateWithDuration:0.1 animations:^{
        [self.hud setAlpha:0.0f];
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            [self.view setAlpha:0.0];
        }completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            [self.keyWindow setUserInteractionEnabled:YES];
        }];
    }];
}

@end
