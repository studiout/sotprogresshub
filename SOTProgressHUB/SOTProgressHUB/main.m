//
//  main.m
//  SOTProgressHUB
//
//  Created by Oni_01 on 14/09/13.
//  Copyright (c) 2013 Andrea Altea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SOTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SOTAppDelegate class]));
    }
}
