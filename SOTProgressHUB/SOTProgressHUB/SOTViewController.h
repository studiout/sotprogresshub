//
//  SOTViewController.h
//  SOTProgressHUB
//
//  Created by Oni_01 on 14/09/13.
//  Copyright (c) 2013 Andrea Altea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOTViewController : UIViewController

- (IBAction)showHud:(id)sender;
- (IBAction)showLoading:(id)sender;


@end
