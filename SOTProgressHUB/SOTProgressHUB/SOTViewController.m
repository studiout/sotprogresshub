//
//  SOTViewController.m
//  SOTProgressHUB
//
//  Created by Oni_01 on 14/09/13.
//  Copyright (c) 2013 Andrea Altea. All rights reserved.
//

#import "SOTViewController.h"

#import "SOTProgressHUB.h"

@interface SOTViewController ()

@end

@implementation SOTViewController

-(BOOL)preferStatusBarHidden{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    
//    //Init with activity indicator
//    //SOTProgressHUB *hub = [[SOTProgressHUB alloc] initWithActivityIndicator];
//    
//    //Init with image
//    SOTProgressHUB *hub = [[SOTProgressHUB alloc] initWithImage:[UIImage imageNamed:@"Favorited"]];
//    
//    //Set Text
//    [hub setText:@"Aggiornamento\n lento"];
//    
//    //show HUD
//    [hub performSelector:@selector(show) withObject:nil afterDelay:1.5f];
//    
//    //Show Hud for defined time
//    //[hub showForTimeInterval:5];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showHud:(id)sender {
    //Init with image
    SOTProgressHUB *hub = [[SOTProgressHUB alloc] initWithImage:[UIImage imageNamed:@"Favorited"]];
    
    //Set Text
    [hub setText:@"Preferito"];
    
    //show HUD
    //[hub performSelector:@selector(show) withObject:nil afterDelay:1.5f];
    
    //Show Hud for defined time
    [hub showForTimeInterval:0.3f];

}

- (IBAction)showLoading:(id)sender {
    //Init with image
    SOTProgressHUB *hub = [[SOTProgressHUB alloc] initWithActivityIndicator];
    
    //Set Text
    [hub setText:@"Loading"];
    
    //show HUD
    //[hub performSelector:@selector(show) withObject:nil afterDelay:1.5f];
    
    //Show Hud for defined time
    [hub showForTimeInterval:10.0f];

}
@end
