//
//  SOTAppDelegate.h
//  SOTProgressHUB
//
//  Created by Oni_01 on 14/09/13.
//  Copyright (c) 2013 Andrea Altea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
