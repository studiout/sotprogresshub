//
//  SOTProgressHUB.h
//  SOTProgressHUB
//
//  Created by Oni_01 on 14/09/13.
//  Copyright (c) 2013 Andrea Altea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SOTProgressHUB : NSObject

@property (strong, nonatomic) UIView *view;
//@property (weak, nonatomic) UIViewController *viewController;

//Init Methods
-(instancetype)initWithImage:(UIImage *)image;
-(instancetype)initWithActivityIndicator;

//Configure Methods
-(void)setImage:(UIImage *)image;
-(void)setText:(NSString *)text;
@property (nonatomic, setter = setMotionEffectsActive:) BOOL motionEffects;

//Show Methods
-(void)show;
-(void)showForTimeInterval:(NSTimeInterval)timeInterval;

//Remove Methods
-(void)remove;

@end
